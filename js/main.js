var camera, scene, renderer;

var mouseX = 0, mouseY = 0;

var windowHalfX = window.innerWidth / 2;
var windowHalfY = window.innerHeight / 2;

var mainObject;

var mixer;
var clock = new THREE.Clock();
var action = {};

var vrControls;
var effect;
var vrMode = false;

var mesh;
var lightTop;
var leftLight;
var rightLight;
var leftLightHelper;
var rightLightHelper;
var lightTopHelper;

var stats = new Stats();
stats.domElement.style.position = 'absolute';
stats.domElement.style.right = '0px';
stats.domElement.style.bottom = '0px';

document.body.appendChild( stats.domElement );

var debugLight = false;
var useLight = true;

var WsConnection;
var headModel = new THREE.Object3D();
var myId;
var otherHeads = [];

var font;

init();
animate();


function toRad(degrees) {
  return degrees * (Math.PI / 180);
}


function init() {

  if (isMobile()) {
    useLight = false;
  }

  renderer = new THREE.WebGLRenderer();
  renderer.setClearColor (0x000000);
  renderer.setPixelRatio( window.devicePixelRatio );
  renderer.setSize( window.innerWidth, window.innerHeight );
  document.body.appendChild( renderer.domElement );

  if (useLight) {
    renderer.shadowMap.enabled = true;
    renderer.shadowMap.type = THREE.BasicShadowMap;
    renderer.physicalLights = true;
  }
  


  camera = new THREE.PerspectiveCamera( 90, window.innerWidth / window.innerHeight, 1, 6000 );
  camera.position.x = 0;
  camera.position.y = 60;
  camera.position.z = 300;
  

  // scene

  scene = new THREE.Scene();

  
  if (useLight) {
    var ambient = new THREE.AmbientLight( 0x444444 );
    ambient.intensity = 0.4;
  } else {
    var ambient = new THREE.AmbientLight( 0xffffff );
    ambient.intensity = 1;
  }
  scene.add( ambient );


  var textureLoader = new THREE.TextureLoader();


  var g = new THREE.BoxGeometry( 200, 10, 200 );
  var m = new THREE.MeshPhongMaterial( {
    color: 0x005500, 
    side: THREE.DoubleSide
  } );

  var plane = new THREE.Mesh( g, m );
  plane.position.set(0, -5, 0);
  if (useLight) {
    plane.receiveShadow = true;
  }
  

  scene.add( plane );


  if (useLight) {
    var lightTop = new THREE.SpotLight( 0xffccff, 17, 300, 0.53, 1);
    lightTop.position.set( 0, 280, 0 );
    lightTop.castShadow = true;
    lightTop.shadow.mapWidth = 128;
    lightTop.shadow.mapHeight = 128;
    scene.add( lightTop );

    if (debugLight) {
      lightTopHelper = new THREE.SpotLightHelper( lightTop );
      scene.add(lightTopHelper);
    }



    leftLight = new THREE.SpotLight( 0xffffff, 5, 360, 0.61, 1);
    leftLight.position.set( -150, 200, 100 );
    leftLight.target.position.set (0, 130, 0);
    leftLight.castShadow = true;
    leftLight.shadow.mapWidth = 128;
    leftLight.shadow.mapHeight = 128;
    scene.add( leftLight );
    scene.add( leftLight.target );

    if (debugLight) {
      leftLightHelper = new THREE.SpotLightHelper( leftLight );
      scene.add(leftLightHelper);
    }



    rightLight = new THREE.SpotLight( 0xffffff, 5, 360, 0.61, 1);
    rightLight.position.set( 150, 200, 100 );
    rightLight.target.position.set (0, 130, 0);
    rightLight.castShadow = true;
    rightLight.shadow.mapWidth = 128;
    rightLight.shadow.mapHeight = 128;
    scene.add( rightLight );
    scene.add( rightLight.target );

    if (debugLight) {
      rightLightHelper = new THREE.SpotLightHelper( rightLight );
      scene.add(rightLightHelper);
    }



    if (debugLight) {
      var gui = new dat.GUI();
      var lf = gui.addFolder('Light');

      function addLightOptions(folder, light) {
        var ltp = folder.addFolder('Target Pos');
        ltp.add(light.target.position, 'x');
        ltp.add(light.target.position, 'y');
        ltp.add(light.target.position, 'z');
        var lposf = folder.addFolder('Light Pos');
        lposf.add(light.position, 'x');
        lposf.add(light.position, 'y');
        lposf.add(light.position, 'z');
        folder.add(light, 'intensity', 0, 1500);
        folder.add(light, 'distance', 0, 2000);
        folder.add(light, 'angle', 0.05, Math.PI/2).step(0.05);
        folder.add(light, 'penumbra', 0, 10).step(1);
        folder.add(light, 'decay', 0, 10).step(1);
      }

      addLightOptions(lf.addFolder('Top'), lightTop);
      addLightOptions(lf.addFolder('Left'), leftLight);
      addLightOptions(lf.addFolder('Right'), rightLight);
    }

  }


  var loader = new THREE.JSONLoader();


  loader.load( './model/women.json', function( geometry, materials ) {


    if (materials) {
      materials.forEach( function ( material ) {
        material.skinning = true;
      });
    }
    

    mesh = new THREE.SkinnedMesh(
      geometry,
      new THREE.MeshFaceMaterial( materials )
    );

    
    if (useLight) {
      mesh.castShadow = true;
    }

    mesh.scale.x = mesh.scale.y = mesh.scale.z = 100;

    mixer  = new THREE.AnimationMixer( mesh );
    action.idle  = mixer.clipAction( geometry.animations[ 0 ] );

    action.idle.setEffectiveWeight( 1 );

    action.idle.play();

    scene.add( mesh );

  } );
  



  //


  //room

  function loadRoom() {
    return new Promise(function (resolve) {
      var mtlLoader = new THREE.MTLLoader();
      mtlLoader.setBaseUrl( 'room/' );
      mtlLoader.setPath( 'room/' );
      mtlLoader.load( 'DRoom.mtl', function( materials ) {
        materials.preload();
        var objLoader = new THREE.OBJLoader();
        objLoader.setMaterials( materials );
        objLoader.setPath( 'room/' );
        objLoader.load( 'DRoom.obj', function ( object ) {
          object.position.y = -75;
          object.rotation.y = Math.PI;
          object.scale.x = object.scale.y = object.scale.z = 0.25;
          scene.add( object );
          resolve();
        });
      });
    });
  };


  //head

  function loadHead() {
    return new Promise(function (resolve) {
      var loader = new THREE.ColladaLoader();
      loader.load( './head/dog.dae', function ( collada ) {
        var head = collada.scene;
        head.scale.x = head.scale.y = head.scale.z = 10;
        head.rotation.z = Math.PI;

        headModel.add(head);
        headModel.position.y = 80;
        headModel.position.z = 80;

        resolve();
      });
    });
  };
  

  function loadFont() {
    return new Promise(function (resolve) {
      var loader = new THREE.FontLoader();
      loader.load( 'fonts/helvetiker_regular.typeface.js', function ( response ) {
        font = response;
        resolve();
      });
    });
  }


  Promise.all([loadRoom(), loadHead(), loadFont()]).then(function(){
    wsConnect();
  });


  effect = new THREE.VREffect(renderer);
  vrControls = new THREE.VRControls(camera);

  document.addEventListener( 'mousemove', onDocumentMouseMove, false );

  //

  window.addEventListener( 'resize', onWindowResize, false );

}

function onWindowResize() {

  windowHalfX = window.innerWidth / 2;
  windowHalfY = window.innerHeight / 2;

  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();

  renderer.setSize( window.innerWidth, window.innerHeight );

}

function onDocumentMouseMove( event ) {

  mouseX = ( event.clientX - windowHalfX ) / 2;
  mouseY = ( event.clientY - windowHalfY ) / 2;

}

//

function animate() {
  stats.begin();

  requestAnimationFrame( animate );

  var delta = clock.getDelta();
  var theta = clock.getElapsedTime();

  if ( mixer ) { mixer.update( delta ); }

  render();

  if (WsConnection && WsConnection.readyState == WsConnection.OPEN) {
    var rotArr = camera.rotation.toArray();
    var rot = {x: rotArr[0], y: rotArr[1], z: rotArr[2]};
    WsConnection.send(JSON.stringify({cmd: 'upd', rot: rot}));
  }

  stats.end();
}

function render() {

  if (vrMode) {
    effect.render(scene, camera);
  }  else {
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.render(scene, camera);
  }

  vrControls.update();
  if (useLight && debugLight) {
    lightTopHelper.update();
    leftLightHelper.update();
    rightLightHelper.update();
  }
  

  
  

}

 function isMobile () {
  var check = false;
  (function (a) {
    if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) {
      check = true;
    }
  })(navigator.userAgent || navigator.vendor || window.opera);
  return check;
};

function requestFullscreen() {
  var el = renderer.domElement;

  if (!isMobile()) {
    effect.setFullScreen(true);
    return;
  }

  if (el.requestFullscreen) {
    el.requestFullscreen();
  } else if (el.mozRequestFullScreen) {
    el.mozRequestFullScreen();
  } else if (el.webkitRequestFullscreen) {
    el.webkitRequestFullscreen();
  }
}

function onWindowResize() {
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();
  if (vrMode) {
    effect.setSize(window.innerWidth, window.innerHeight);
  } else {
    renderer.setSize(window.innerWidth, window.innerHeight);
  }
}

function onFullscreenChange(e) {
  var fsElement = document.fullscreenElement ||
    document.mozFullScreenElement ||
    document.webkitFullscreenElement;

  if (!fsElement) {
    vrMode = false;
  } else {
    // lock screen if mobile
    window.screen.orientation.lock('landscape');
  }
}

function onIncomingMessage(data, evt) {

  if (data.addId && data.addId != myId) {
    //console.log('on add id', data.addId);
    spawnNewHead(data.addId, data.headInfo);
  }

  if (data.removeId) {
    //console.log('on remove id', data.removeId);
    removeHead(data.removeId);
  }

  if (data.joinId) {
    //console.log("onJoin", data, evt.data);
    onMyJoin(data);
  }

  if (data.rots) {
    onUpdateHeadsRots(data);
  }
}

function onMyJoin(data) {
  myId = data.joinId;
  var pos = data.headInfo.pos;
  camera.position.set(pos.x, pos.y, pos.z);

  for (var id in data.allPlayers) {
    if (data.allPlayers.hasOwnProperty(id) && myId != id) {
      var headInfo = data.allPlayers[id];
      spawnNewHead(id, headInfo);
    }
  }

  spawnNewHead(myId, data.headInfo);

  if (otherHeads[myId]) {
    scene.remove(otherHeads[myId]);
    delete otherHeads[myId];
    //console.log('hide my');
  }
}

function onUpdateHeadsRots(data) {
  for (var id in data.rots) {
    if (data.rots.hasOwnProperty(id) && otherHeads[id]) {
      var rot = data.rots[id];
      otherHeads[id].rotation.set(rot.x, rot.y, rot.z);
      //console.log('update rot', id, rot);
    }
  }
}

function spawnNewHead(id, headInfo) {
  if (!otherHeads[id]) {
    var head = headModel.clone();
    head.position.set(headInfo.pos.x, headInfo.pos.y, headInfo.pos.z);
    head.rotation.set(headInfo.rot.x, headInfo.rot.y, headInfo.rot.z);
    otherHeads[id] = head;
    scene.add( head );

    var label = textSprite(headInfo.nickname);
    label.position.x += 6;
    label.position.y += 14;
    label.rotation.x = 0;
    label.rotation.y = Math.PI;
    head.add(label);

    //console.log('spawn new other head', id, headInfo, head);
  }
}

function removeHead(id) {
  if (otherHeads[id]) {
    var head = otherHeads[id];
    scene.remove(head);
    delete otherHeads[id];
  }
  //console.log('remove head', id);
}

function wsConnect() {

  document.getElementById('connecting').style.display = "block";
  document.getElementById('disconnected').style.display = "none";

  WsConnection = new WebSocket("wss://mpvr-server.getid.org");

  WsConnection.onopen = function () {
      document.getElementById('disconnected').style.display = "none";
      document.getElementById('connecting').style.display = "none";
  }

  WsConnection.onclose = function (evt) {
      document.getElementById('disconnected').style.display = "block";
      document.getElementById('connecting').style.display = "none";
      window.setTimeout(function(){
          location.reload();
      }, 3000);
  };

  WsConnection.onmessage = function (evt) {
      var messages = evt.data.split('\n');
      for (var i = 0; i < messages.length; i++) {
          var json;
          try {
              json = JSON.parse(messages[i]);
              
          } catch (ex) {
              console.warn("Json parse error", evt.data, ex);
          }
          if (json) {
              onIncomingMessage(json, evt);
          }
      }
      
  };

};


function textSprite(text) {

    var textGeo = new THREE.TextGeometry(text, {
          font: font,
          size: 4,
          height: 0.3
        });
        textGeo.computeBoundingBox();
        textGeo.computeVertexNormals();

        var material = new THREE.MeshBasicMaterial( { color: 0x888888 } );

        var textMesh = new THREE.Mesh( textGeo, material );
        return textMesh;
}



document.querySelector('#enterVr').addEventListener('click', function() {
    vrMode = vrMode ? false : true;
    requestFullscreen();
    onWindowResize();
});


document.addEventListener('fullscreenchange', onFullscreenChange);
document.addEventListener('mozfullscreenchange', onFullscreenChange);
window.addEventListener('resize', onWindowResize, false );
